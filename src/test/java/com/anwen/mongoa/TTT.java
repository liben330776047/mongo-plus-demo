/*
 * Copyright (c) JiaChaoYang 2023-7-27 MongoPlus版权所有
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * email: j15030047216@163.com
 * phone: 15030047216
 * weChat: JiaChaoYang_
 */

package com.anwen.mongoa;

import com.anwen.mongo.mapper.MongoPlusMapMapper;
import com.anwen.mongoa.entity.User;
import com.anwen.mongoa.service.RoleService;
import com.anwen.mongoa.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import javax.annotation.Resource;

/**
 * @author JiaChaoYang
 **/
@SpringBootTest
public class TTT{

    @Resource
    private UserService userService;

    @Resource
    private MongoTemplate mongoTemplate;

    @Resource
    private RoleService roleService;

    @Resource
    private MongoPlusMapMapper mongoPlusMapMapper;

    @Test
//    @MongoTransactional
    public void m(){

        userService.lambdaQuery().likeLeft(User::getUserName,"张").list().forEach(System.out::println);

        /*userService.updateById(new User(){{
            setUserId(1L);
            setUserName("李四");
        }});*/
        /*List<Map<String, Object>> user = mongoPlusMapMapper.list("user");
        System.out.println(user);*/
//        List<User> userList = userService.lambdaAggregate()
//                .limit(10)
//                .skip(1)
////                .custom(new AggregateBasicDBObject("$"+AggregateTypeEnum.UNION_WITH.getType(),"user",2))
//                .unionWith("user")
//                .list();
//        List<User> userList = userService.lambdaQuery().and(userService.lambdaQuery().or(userService.lambdaQuery().like(User::getUserName, "张").like(User::getUserName, "三")).eq(User::getUserName,"张三")).list();
//        userList.forEach(System.out::println);
//        Boolean b = userService.removeById(1);
//        List<User> userList = userService.lambdaQuery().eq(User::getUserName,"张三").orderByDesc(User::getCreateTime).list();
//        mongoTemplate.findAll(User.class,"user").forEach(System.out::println);
/*        List<User> userList = userService.list();
        userList.forEach(user -> {
            user.setUserAge(1000);
        });
        User user = userList.get(0);
        user.setUserId(null);
        userList.add(user);
        userList.remove(0);
        Boolean b = userService.saveOrUpdateBatch(userList);
        System.out.println(b);*/
//        User user = new User();
//        user.setUserId("1746894180976537602L");
//        user.setUserName("佳佳");
//        Boolean b = userService.saveOrUpdateWrapper(user, userService.lambdaQuery().eq(User::getUserId, "1746894180976537602"));
//        System.out.println(b);
//        mongoTemplate.findAll(User.class,"user").forEach(System.out::println);
//        userService.list().forEach(System.out::println);
//        List<JSONObject> mongoTemplateAll = mongoTemplate.findAll(JSONObject.class, "user");
//        System.out.println(JSON.toJSONString(mongoTemplateAll));

//
//        List<Map<String,Object>> mapList = new ArrayList<>();
//        mapList.add(new HashMap<String, Object>() {{
//            put("name", "张三");
//            put("datetime", new Date());
//        }});
//        mapList.add(new HashMap<String, Object>() {{
//            put("name", "李四");
//            put("datetime", new Date());
//        }});
//        Boolean save = mongoPlusMapMapper.saveBatch("test", mapList);
//        System.out.println(save);

//        userService.lambdaUpdate().eq(User::getUserId,1).update();
//        User user = userService.getById(7);
//        System.out.println(user);

//        List<User> userList = userService.lambdaQuery().eq(User::getUserId, 1).list();
//        userList.forEach(System.out::println);
//        Boolean save = mongoPlusMapMapper.save("codecTest", );
//        System.out.println(save);
        /*List<Role> roleList = roleService.lambdaAggregate().group(Role::getName).list();
        roleList.forEach(System.out::println);*/
        /*Role role = roleService.lambdaQuery().orderByDesc(Role::getAge).one();
        System.out.println(role);*/
        /*boolean update = roleService.lambdaUpdate().set(Role::getName, "超级管理员").eq(Role::getId, 1699432824774127617L).remove();
        System.out.println(update);*/
//        Role role = new Role();
//        role.setName("超级管理员");
//        role.setAge(19);
//        T t = new T();
//        t.setRoleName("角色内部名称");
//        t.setRoleStatus(1);
//        role.setRole(t);
//        roleService.save(role);
//        List<Map<String, Object>> mapList = mongoPlusMapMapper.lambdaAggregate().match(new InjectWrapper().like("name", "角色")).allowDiskUse(true).list("test");
//        List<Role> roleList = roleService.lambdaAggregate().match(new Wrapper<Role>().like(Role::getName, "角色")).allowDiskUse(true).list();
//        System.out.println(mapList.get(0));
//        List<User> roleList = userService.lambdaQuery().eq(User::getUserStatus,1).list();
//        List<Map<String, Object>> mapList = mongoPlusMapMapper.list("test");
//        System.out.println(JSONUtil.toJsonStr(roleList));
//        roleService.save(new Role());
//        InjectAggregateWrapper injectAggregateWrapper = new InjectAggregateWrapper();
//        List<Map<String, Object>> mapList = mongoPlusMapMapper.aggregateList("test", injectAggregateWrapper.match(mongoPlusMapMapper.lambdaQuery().eq("name", "超级管理员")));
//        System.out.println(JSONUtil.toJsonStr(mapList));
//        List<Map<String, Object>> mapList = mongoPlusMapMapper.lambdaAggregate().match(mongoPlusMapMapper.lambdaQuery().eq("name", "超级管理员")).list("test");
//        List<Role> roleList = roleService.lambdaAggregateChain().replaceRoot(true,Role::getRole).list();
//        System.out.println(JSONUtil.toJsonStr(roleList));
    }

}
