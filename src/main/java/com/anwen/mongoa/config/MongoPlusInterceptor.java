/*
 * Copyright (c) JiaChaoYang 2024-3-17 MongoPlus版权所有
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * email: j15030047216@163.com
 * phone: 15030047216
 * weChat: JiaChaoYang_
 */

package com.anwen.mongoa.config;

import com.anwen.mongo.interceptor.Interceptor;
import com.anwen.mongo.model.MongoPlusDocument;
import com.anwen.mongoa.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author JiaChaoYang
 **/
@Configuration
@Slf4j
public class MongoPlusInterceptor implements Interceptor {
    @Override
    public List<MongoPlusDocument> executeSave(List<MongoPlusDocument> documentList) {
        log.info("动态代理值打印");
        documentList.forEach(document -> {
            System.out.println(document);
            document.put(User::getUserName,"娇娇娇娇");
        });
        return documentList;
    }
}
