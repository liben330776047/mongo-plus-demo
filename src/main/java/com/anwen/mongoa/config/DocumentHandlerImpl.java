/*
 * Copyright (c) JiaChaoYang 2024-1-15 MongoPlus版权所有
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * email: j15030047216@163.com
 * phone: 15030047216
 * weChat: JiaChaoYang_
 */

package com.anwen.mongoa.config;

import com.alibaba.fastjson.JSON;
import com.anwen.mongo.handlers.DocumentHandler;
import org.bson.Document;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author JiaChaoYang
 **/
@Component
public class DocumentHandlerImpl implements DocumentHandler {
    @Override
    public List<Document> insertInvoke(List<Document> document) {
//        System.out.println(JSON.toJSONString(document));
        return document;
    }

    @Override
    public List<Document> updateInvoke(List<Document> document) {
//        System.out.println(JSON.toJSONString(document));
        return document;
    }
}
