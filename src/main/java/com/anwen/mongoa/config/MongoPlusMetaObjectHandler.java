/*
 * Copyright (c) JiaChaoYang 2023-11-21 MongoPlus版权所有
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * email: j15030047216@163.com
 * phone: 15030047216
 * weChat: JiaChaoYang_
 */

package com.anwen.mongoa.config;

import com.anwen.mongo.handlers.MetaObjectHandler;
import org.bson.Document;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;

/**
 * @author JiaChaoYang
 **/
//@Component
public class MongoPlusMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(Map<String, Object> insertFillMap, Document document) {
        insertFillMap.keySet().forEach(key -> {
            if (key.equals("createTime")){
                document.put(key, LocalDateTime.now());
            }
        });
    }

    @Override
    public void updateFill(Map<String, Object> insertFillMap, Document document) {
        insertFillMap.keySet().forEach(key -> {
            if (key.equals("createTime")){
                document.put(key, LocalDateTime.now());
            }
        });
    }

    public static void createAsciiPic(final String path) {
        final String base = "@#&$%*o!;.";// 字符串由复杂到简单
//        final String base = "KSPksp;.";
        try {
            final BufferedImage image = ImageIO.read(new File(path));
            //System.out.println("W:"+image.getWidth()+" H:"+image.getHeight());
            for (int y = 0; y < image.getHeight(); y += 2) {
                for (int x = 0; x < image.getWidth(); x++) {
                    final int pixel = image.getRGB(x, y);
                    final int r = (pixel & 0xff0000) >> 16, g = (pixel & 0xff00) >> 8, b = pixel & 0xff;
                    final float gray = 0.299f * r + 0.578f * g + 0.114f * b;
                    final int index = Math.round(gray * (base.length() + 1) / 255);
                    System.out.print(index >= base.length() ? " " : String.valueOf(base.charAt(index)));
                }
                System.out.println();
            }
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args){
        createAsciiPic("C:\\Users\\Administrator\\Desktop\\未标题-1.png");
    }

}
